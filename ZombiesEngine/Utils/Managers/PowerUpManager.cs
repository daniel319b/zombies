﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ZombiesEngine
{
    /// <summary>
    /// A helper class that Updates the Power ups in the game, it also provides a method for adding a random Power up.
    /// </summary>
    public class PowerUpManager
    {
        static List<PowerUp> powerUps = new List<PowerUp>();

        /// <summary>
        /// Updates the Power ups.
        /// </summary>
        /// <param name="p">The player of the game</param>
        /// <param name="gameTime">The gameTime.</param>
        public static void Update(Player p,GameTime gameTime)
        {
            UpdatePowerUps(p,gameTime);
        }

        /// <summary>
        /// Updates the Power Ups.
        /// </summary>
        private static void UpdatePowerUps(Player player,GameTime gameTime)
        {
            List<PowerUp> inactive = new List<PowerUp>(powerUps.Count);
            foreach (PowerUp p in powerUps)
            {
                p.Update(gameTime);

                if (p.Alive == false && p.EffectIsActive == false)
                    inactive.Add(p);
            }
            foreach (PowerUp p in inactive)
                powerUps.Remove(p);
            inactive.Clear();
        }

        /// <summary>
        /// Adds a new random Power Up to the game.
        /// </summary>
        /// <param name="z">The Power Up will spawn on the dead zombie you pass in.</param>
        public static void AddPowerUp(Zombie z,Player player)
        {
            Random random = new Random();
            int r = random.Next(1, 100);
            if (r >= 80)//20 percent
            {
                r = random.Next(1, 100);
                PowerUp p;
                if (r >= 70)
                    p = new PowerUps.Health(player, 10, 10);
                else if (r >= 1 && r <= 30)
                    p = new PowerUps.DoublePoints(player, 5, 10);
                else
                    p = new PowerUps.Ammo(player, 5, 1);

                p.Position = z.Position;
                powerUps.Add(p);
            }
        }

        public static void Draw(SpriteBatch sb)
        {
            foreach (PowerUp p in powerUps)
                p.Draw(sb);
        }
    }
}
