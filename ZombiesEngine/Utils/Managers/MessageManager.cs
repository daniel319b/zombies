﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ZombiesEngine
{
    /// <summary>
    /// This is a helper class for displaying fading messages in the game.
    /// </summary>
    public abstract class MessageManager
    {
        static List<GameObject> messages = new List<GameObject> ();

        private MessageManager() {}
        /// <summary>
        /// Updates the messages.
        /// </summary>
        public static void Update()
        {    
            UpdateMessages();
        }

        /// <summary>
        /// Adds a new message to the game.
        /// </summary>
        /// <param name="message">The Message game object to add.</param>
        public static void AddMessage(GameObject message)
        {
            messages.Add(message);
        }

        /// <summary>
        /// Adds a new message to the game.
        /// </summary>
        public static void AddMessage(string message,Vector2 position,Color color)
        {
            GameObject msg = new GameObject(position)
                                 {
                                     Color = color,
                                     Velocity = new Vector2(0, -2),
                                     Text = message
                                 };
            messages.Add(msg);
        }
        /// <summary>
        /// Draws all the messages on screen.
        /// </summary>
        /// <param name="sb">A spriteBatch.</param>
        public static void Draw(SpriteBatch sb)
        {
            foreach (GameObject message in messages)
                message.Draw(sb, Camera.Transform);     
        }

        /// <summary>
        /// Updates the Score Messages.
        /// </summary>
        private static void UpdateMessages()
        {
            List<GameObject> inactiveMessages = new List<GameObject>(messages.Count);//holds the inactive objects to be removed.
            foreach (GameObject message in messages)
            {
                message.Position += message.Velocity;
                message.Opacity -= 0.008f;//fades out the message.         
                if (message.Opacity < 0.01f)
                    inactiveMessages.Add(message);
            }
            foreach (GameObject message in inactiveMessages)
                messages.Remove(message);//remove current inactive object.
            inactiveMessages.Clear();
        }


        /// <summary>
        /// Creates a floating message that will appear at the dead zombie and fade out.
        /// </summary>
        /// <param name="message">The message you want to be displayed.</param>
        public static void addNewScoreMessage(string message, Zombie z)
        {
            GameObject scoreObj = new GameObject();
            scoreObj.Text = message;
            scoreObj.Color = Color.Yellow;
            scoreObj.Velocity = new Vector2(0, -2);
            scoreObj.Position = new Vector2(z.Position.X, z.Position.Y);
            MessageManager.AddMessage(scoreObj);
        } 
    }
}
