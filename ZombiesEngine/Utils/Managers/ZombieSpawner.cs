﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ZombiesEngine
{
    /// <summary>
    /// Helper class the manages the spawning of the zombies.
    /// </summary>
    public class ZombieSpawner
    {
        #region Fields and Properties

        List<Zombie> zombies;
        /// <summary>
        /// The list of current live zombies in the game.
        /// </summary>
        public List<Zombie> Zombies
        {
            get { return zombies; }
            set { zombies = value; }
        }
        int spawnTime = 1;

        /// <summary>
        /// The spawn time of the zombies.
        /// </summary>
        public int SpawnTime
        {
            get { return spawnTime; }
            set { spawnTime = value; }
        }

        Viewport viewport;
        float time;
        Random rnd;

        #endregion

        #region Constructor
        /// <summary>
        /// Constructs a new Zombie Spawner.
        /// </summary>
        /// <param name="view">A viewport object.</param>
        public ZombieSpawner(Viewport view)
        {
            viewport = view;
            zombies = new List<Zombie>();
            rnd = new Random();
        }
        #endregion

        #region Update and Draw Methods
        /// <summary>
        /// Updates the spawner, spawns zombies.
        /// </summary>
        /// <param name="gameTime">A gameTime Object.</param>
        /// <param name="player">The player of the game.</param>
        public void Update(GameTime gameTime,GameObject player)
        { 
            time += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (time >= spawnTime)
            {
                SpawnZombie();
                time = 0;
                spawnTime = rnd.Next(1,2);
            }
            UpdateZombies(gameTime, player);
        }

        private void UpdateZombies(GameTime gameTime, GameObject player)
        {
            for (int i = 0; i < zombies.Count; i++)
            {
                zombies[i].Update(player, gameTime);//Updates every zombie

                for (int j = 0; j < zombies.Count; j++)
                {
                    zombies[i].Flock(zombies[j], 60.0f);//Apply flocking
                }
                if (zombies[i].Alive == false)//if the zombie is dead,remove it.
                    zombies.RemoveAt(i);
            }
        }

        /// <summary>
        /// Spawns a random zombie at a random position.
        /// </summary>
        private void SpawnZombie()
        {
            Zombie z;
            int r = rnd.Next(1, 100);

            if (r >= 60)// 40 percent of spawning this zombie.
                z = new Zombie();
            else if (r >= 30 && r < 60)
                z = new BigZombie();
            else
                z = new FastZombie();

            if (z is BigZombie)
                z.Texture = TextureManager.SetTexture("Zombie/BigZombie");
            else if(z is FastZombie)
                z.Texture = TextureManager.SetTexture("Zombie/Zombie2");
            else 
                z.Texture = TextureManager.SetTexture("Zombie/Zombie");
        
            RandomizePosition(z);
            zombies.Add(z);
        }

        /// <summary>
        /// Randomizes a position at the edges of the screen, for the zombie.
        /// </summary>
        private void RandomizePosition(Zombie z)
        {
            int r = rnd.Next(1, 4);//used for checking,1 is up,2 is down,3 is left,4 is right.
            Vector2 location = Vector2.Zero;
            if (r == 1 || r == 2)//if up or down
            {
                //randomize the x position.
                location.X = rnd.Next(0, viewport.Width);
                if (r == 1)//if is up 
                    location.Y = 0;
                location.Y = viewport.Height;//if is down.
            }
            else if (r == 3 || r == 4)//if is left or right.
            {
                location.Y = rnd.Next(0, viewport.Height);
                if (r == 3)
                    location.X = 0;
                location.X = viewport.Width;
            }
            z.Position = location;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Zombie z in zombies)
                z.Draw(spriteBatch);
        }
        #endregion
    }
}
