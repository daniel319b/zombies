﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZombiesEngine.Skills_and_Upgrades
{
    /// <summary>
    /// This is an event argument extension class for Upgrades.
    /// </summary>
    public class UpgradeEventArgs : EventArgs
    {
        public PlayerUpgrade upgrade;

        public UpgradeEventArgs(PlayerUpgrade upgrade)
        {
            this.upgrade = upgrade;
        }
    }

    /// <summary>
    /// Upgrade the played with the specified upgrade.
    /// </summary>
    /// <param name="upgrade">The upgrade</param>
    delegate void UpgradePlayer(PlayerUpgrade upgrade);

    /// <summary>
    /// Static class that holds all the upgrade effects in the game. Add custome effects here.
    /// </summary>
    public static class UpgradesEffects
    {
        /// <summary>
        /// All the upgrade effects in the game. Call an upgrade - effects[upgradeName]();
        /// </summary>
        public static Dictionary<string, UpgradePlayer> effects = new Dictionary<string, UpgradePlayer>()
        {
            { "Health", UpgradeHealth },
            { "Weapon Damage", UpgradeWeaponDamage },
            { "Grenade Damage", UpgradeGranadeDamage },
            { "Reload Speed", UpgradeReloadSpeed }
        };

        static void UpgradeHealth(PlayerUpgrade upgrade)
        {
            int maxhealth = Session.Singleton.Player.CurrentStatistics.MaxHealth;
            maxhealth += (int)(maxhealth + maxhealth * upgrade.UpgradePercent);
            Session.Singleton.Player.CurrentStatistics.MaxHealth = maxhealth;
        }

        static void UpgradeWeaponDamage(PlayerUpgrade upgrade)
        {

        }

        static void UpgradeGranadeDamage(PlayerUpgrade upgrade)
        {

        }

        static void UpgradeReloadSpeed(PlayerUpgrade upgrade)
        {

        }
    }
}
