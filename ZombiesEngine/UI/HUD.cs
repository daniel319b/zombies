﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ScreenSystem;

namespace ZombiesEngine
{
    /// <summary>
    /// A Helper class that is used to draw the hud/the player's stats, on the screen.
    /// </summary>
    public static class HUD
    {
        public static Player Player
        { get; set; }

        public static void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            drawHealthBar(spriteBatch);
            drawWeaponPicture(spriteBatch);
            drawHealthPictures(spriteBatch);
            drawBulletHUD(spriteBatch);
            spriteBatch.End();
            DrawTextStats();
        }

        #region Draw Methods
        private static void drawHealthPictures(SpriteBatch sb)
        {
            if (Player.healthPicture.Texture != null)
                sb.Draw(Player.healthPicture.Texture, Player.healthPicture.Position, Color.White);
        }

        private static void drawWeaponPicture(SpriteBatch sb)
        {
            if (Player.Weapon is Pistol)//draw the pistol pic                        
                sb.Draw(TextureManager.SetTexture("Player/pistol"), new Vector2(1180, 630), Color.White);
            else if (Player.Weapon is Shotgun)//draw the shotgun pic.                          
                sb.Draw(TextureManager.SetTexture("Player/shotgun"), new Vector2(1140, 640), Color.White);
        }

        private static void drawHealthBar(SpriteBatch sb)
        {
            sb.Draw(Player.healthBar, new Rectangle(110, 10, Player.healthBar.Width, 44), new Rectangle(0, 0, Player.healthBar.Width, 44), Color.Gray);
            sb.Draw(Player.healthBar, new Rectangle(110, 10, (int)(Player.healthBar.Width * ((double)Player.Health / Player.MaxHealth)), 44), new Rectangle(0, 0, Player.healthBar.Width, 44), Color.Red);
        }

        private static void drawBulletHUD(SpriteBatch sb)
        {
            for (int i = 0; i < Player.Weapon.BulletsLeft; i++)
                sb.Draw(TextureManager.SetTexture("Player/BulletHUD"), new Vector2(1150 - i * 10, 660), Color.Black);
        }

        private static void DrawTextStats()
        {
            Vector2 leftCorner = new Vector2(0, Camera.ScreenBounds.Y - 25);
         
            ScreenWriter.Write("Score: " + Player.Score, new Vector2(5, 85), Color.Black);
            ScreenWriter.Write("Bullets left: " + (Player.Weapon.BulletsLeft), new Vector2(5, 110), Color.Black);
            ScreenWriter.Write("Magazines left: " + (Player.Weapon.Magazines), new Vector2(5, 130), Color.Black);
            ScreenWriter.Write("Health: " + Player.Health, new Vector2(Player.healthBar.Width + 10, 20), Color.Yellow); 
            if (Player.Weapon.BulletsLeft == 0) ScreenWriter.Write("RELOAD", Input.MousePosition, Color.Red);    
        }
        #endregion
    }
}
