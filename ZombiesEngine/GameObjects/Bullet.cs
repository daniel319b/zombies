﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ZombiesEngine
{
    public class Bullet : GameObject 
    {
        public Bullet()
        {
          Texture = TextureManager.SetTexture("Player/bullet");
        }

        public override void Update()
        {   
            rotation = (float)Math.Atan2(velocity.Y, velocity.X);      
            Position += velocity;
            base.Update();
        }  

        public override void Draw(SpriteBatch sb)
        {
            sb.Begin(SpriteSortMode.Deferred, null, null, null, null, null, Camera.Transform);
            sb.Draw(texture, position, null, Color.Yellow, rotation, Vector2.Zero, 1, SpriteEffects.None, 0);
            sb.End();
        }
    }
}
