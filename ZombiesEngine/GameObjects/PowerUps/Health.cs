﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZombiesEngine.PowerUps
{
    /// <summary>
    /// This class represents a Health power up which will give the player a bonus of health on pickup.
    /// </summary>
    public class Health : PowerUp
    {
        int healthBonus;

        /// <summary>
        /// Constructs a new Health PowerUp
        /// </summary>
        /// <param name="p">The player.</param>
        /// <param name="health">A health Bonus to the player.</param>
        /// <param name="lifeTime" >How much time(seconds) will the power up be alive.</param>
        public Health(Player p,int healthBonus,float lifeTime):base(p,lifeTime)
        {
            this.healthBonus = healthBonus;
            Texture = TextureManager.SetTexture("PowerUps/Health");
        }

        protected override void Activate()
        {
            player.Health += healthBonus;
            if (player.Health > player.MaxHealth)
                player.Health = player.MaxHealth;      
        }
    }
}
