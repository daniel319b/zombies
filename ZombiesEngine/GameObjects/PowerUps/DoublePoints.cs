﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ScreenSystem;

namespace ZombiesEngine.PowerUps
{
    /// <summary>
    /// This class represets a Double Points power up which will give the player a bonus of points for an amount of time.
    /// </summary>
    public class DoublePoints : PowerUp
    {
        /// <summary>
        /// Creates a new Double Points power up.
        /// </summary>
        /// <param name="p">The player in the game.</param>
        /// <param name="lifeTime">How much time(seconds) will the power up be alive.</param>
        /// <param name="effectTime">How long will the double points effect last.</param>
        public DoublePoints(Player p,float lifeTime,float effectTime) : base(p,lifeTime)
        { 
            EffectTime = effectTime;
            Texture = TextureManager.SetTexture("PowerUps/DoublePoints");     
        }
 
       
        protected override void Activate()
        {
            player.ScoreMultiplier = 2;
        }

        protected override void Deactivate()
        {
            player.ScoreMultiplier = 1;
        }


        public override void Draw(SpriteBatch sb)
        {
            base.Draw(sb);
            if (EffectIsActive)
            {
                sb.Begin();
                sb.DrawString(Font.Regular, "Double Points Time:" + (int)(EffectTime - 0), new Vector2(700, 690), Color.Black);
                sb.End();
            }
        }
    }
}
