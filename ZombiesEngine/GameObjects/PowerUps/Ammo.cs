﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZombiesEngine.PowerUps
{
    /// <summary>
    /// This class represents an Ammo power up which gives the player some ammo on pickup.
    /// </summary>
    public class Ammo : PowerUp
    {
        /// <summary>
        /// How much ammo to give to the player.(Magazines)
        /// </summary>
        public int AmmoToGive
        {
            get;
            set;
        }
    
        /// <summary>
        /// Creates an ammo power up.
        /// </summary>
        /// <param name="p">The player of the game.</param>
        /// <param name="lifeTime">How much time(seconds) will the power up be alive.</param>
        /// <param name="ammo">The amount of ammo which it will give te player on pickup.</param>
        public Ammo(Player p, float lifeTime,int ammo) : base(p, lifeTime)
        {
            AmmoToGive = ammo;
            Texture = TextureManager.SetTexture("PowerUps/Ammo");
        }
                  

        protected override void Activate()
        {
            player.Weapon.Magazines += AmmoToGive;
        }
    }
}
