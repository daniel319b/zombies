﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ZombiesEngine
{
    /// <summary>
    /// This class represents a pickable power up which randomly spawns when on a zombie's death.
    /// </summary>
    public class PowerUp : GameObject
    {
        #region Fields and Properties
        protected Player player;
        float lifeTime = 0, maxLifeTime = 0;
        float effectTime = 0,currentEffectTime;
        public  bool EffectIsActive;
       
        
        /// <summary>
        /// The maximum life time, in seconds.
        /// </summary>
        public float MaxLifeTime
        {
            get { return maxLifeTime; }
            set { maxLifeTime = value; }
        }

        /// <summary>
        /// The current life time, in Seconds.
        /// </summary>
        public float LifeTime
        {
            get { return lifeTime; }
            set { lifeTime = value; }
        }

        /// <summary>
        /// How long will the double points power up last. (seconds)
        /// </summary>
        public float EffectTime
        {
            get { return effectTime; }
            set { effectTime = value; }
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Constructs a new PowerUp object.
        /// </summary>
        /// <param name="p">A player.</param>
        /// <param name="lifeTime">How much time(seconds) will the power up be alive.</param>
        public PowerUp(Player p,float lifeTime)
        {
            player = p;
            maxLifeTime = lifeTime;
        }

        #endregion

        #region Update and Draw Methods
        /// <summary>
        /// Updates the PowerUp
        /// </summary>
        public virtual void Update(GameTime gameTime)
        {
            if (isPickedByPlayer())
            {
                Activate();
                EffectIsActive = true;
                Alive = false;
            }
         
                checkForEffectFinish(gameTime);
            checkDeath(gameTime);
            base.Update();
        }

        private void checkForEffectFinish(GameTime gameTime)
        {
            if (currentEffectTime > EffectTime)//if the power up's effect time has passed.
            {
                Deactivate();//Deactivate the effect.
                EffectIsActive = false;//The effect is not active.
                return;
            }
            float timer = (float)gameTime.ElapsedGameTime.TotalSeconds;
            currentEffectTime += timer;
        }

        /// <summary>
        /// Activates the PowerUp and the player is effected by it.
        /// </summary>
        protected virtual void Activate()
        {
            
        }

        protected virtual void Deactivate()
        {

        }
        /// <summary>
        /// Checks if the player took the power up.
        /// </summary>
        /// <returns></returns>
        protected bool isPickedByPlayer()
        {
            if (Alive && player.BoundBox.Intersects(this.BoundBox))
                return true;
            return false;
        }

        /// <summary>
        /// Checks if the power up expired.
        /// </summary>
        protected void checkDeath(GameTime gameTime)
        {
            float time = (float)gameTime.ElapsedGameTime.TotalSeconds;
            lifeTime += time;

            if (lifeTime >= maxLifeTime)
                Alive = false;          
        }

           
        /// <summary>
        /// Draws the power up object.
        /// </summary>
        /// <param name="sb">A spriteBatch</param>
        public override void Draw(SpriteBatch sb)
        {
            if (Alive)
            {
                sb.Begin(SpriteSortMode.Deferred, null, null, null, null, null, Camera.Transform);
                sb.Draw(Texture, Position, null, Color.White, Rotation, Origin, 1.0f, SpriteEffects.None, 0.0f);
                sb.End();
            }
        }
        #endregion

    }
}
