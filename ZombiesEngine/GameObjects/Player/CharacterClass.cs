﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZombiesEngine.GameObjects.Player
{
    public class CharacterClass
    {
        public int Level { get; set; }

        public List<Ability> AvailableAbilities { get; set; }

        public int RemainingSkillPoints { get; set; }

        
    }

    
}
