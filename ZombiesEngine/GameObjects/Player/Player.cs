﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ScreenSystem;

namespace ZombiesEngine
{
    /// <summary>
    /// Represents The Player Of The Game.
    /// </summary>
    public class Player : GameObject
    {
        public int Level { get; set; }

        private int experiencePoints;

        public int ExperiencePoints
        {
            get { return experiencePoints; }
            set
            { 
                experiencePoints = value;
                if (experiencePoints >= 50 * (Level + 1)) 
                    Level++;
            }
        }
        public int MaxHealth { get; set; }

        #region Fields and Properties
        private Weapon weapon;//the current weapon.     
        private int score;
        private int scoreMultiplier = 1;
        private int scoreToAdd; 
        private Weapon prevWeapon;//the previous weapon.
        public GameObject healthPicture;

        public static Texture2D healthBar, crosshair;

        List<Grenade> grenades = new List<Grenade>();
        /// <summary>
        /// The score of the player.
        /// </summary>
        public int Score
        {
            get { return score; }
            set { score = value >= 0 ? value : 0; }
        }

        /// <summary>
        /// The current weapon of the player.
        /// </summary>
        public Weapon Weapon
        {
            get { return weapon; }
            set { weapon = value; }
        }

        /// <summary>
        /// The score multiplier of the player.
        /// </summary>
        public int ScoreMultiplier
        {
            get { return scoreMultiplier; }
            set { scoreMultiplier = value; }
        }   

        /// <summary>
        /// The current score to add.
        /// </summary>
        public int ScoreToAdd
        {
            get { return scoreToAdd; }
            set { scoreToAdd = value * scoreMultiplier; }
        }

        /// <summary>
        /// How many zombies killed.
        /// </summary>
        public int ZombiesKilled { get; set; }

        
        /// <summary>
        /// The amount of time that the player was alive.(seconds)
        /// </summary>
        public float TimeAlive
        {
            get;
            private set;
        }
        #endregion


        #region Initialization
        /// <summary>
        /// Creates a new Player.
        /// </summary>
        public Player()
        {
            MaxHealth = health = 100;//Set the start Health to 100.
            weapon = new Pistol (this);//Set the start weapon to a Pistol.
            healthPicture = new GameObject(Vector2.Zero);
            healthBar = TextureManager.SetTexture("Player/HealthBar");
            crosshair = TextureManager.SetTexture("Player/crosshair");
            texture = TextureManager.SetTexture("Player/Player_Pistol");
            ResetWeapons();
        }

        public void ResetWeapons()
        {
            weapon = new Pistol(this);
          //  prevPistol = new Pistol(this);
           // prevShotgun = new Shotgun(this);
            prevWeapon = new Shotgun (this);
        }
        #endregion



        #region Update Methods
        /// <summary>
        /// Upadates the player's movement and shooting. 
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {          
            handleInput(gameTime);//Handles movement,
            weapon.Update(gameTime);//Updates the weapon.     
            updateHealthPictures();//Updates the health picture by the current health.
            if (Alive == false)//if the player is dead, reset it.
                ResetPlayer();
            Alive = health > 0;//Checks if alive.
            Camera.Position = position;//Sets the Camera to follow the player.
            TimeAlive += (float)gameTime.ElapsedGameTime.TotalSeconds;
            foreach (Grenade g in grenades)
                g.Update(gameTime);
         base.Update();
        }

        /// <summary>
        /// Resets the player position,score, and health...
        /// </summary>
        public void ResetPlayer()
        {
            Alive = true;
            Health = 100;
            score = 0;
            Vector2 middleScreen = new Vector2((Camera.ScreenBounds.Width / 2) - Texture.Width / 2, (Camera.ScreenBounds.Height / 2) - Texture.Height / 2);
            Position = middleScreen;
            TimeAlive = 0;
            ResetWeapons();
        }    
        /// <summary>
        /// Updates the health pictures depending on the player's current health.
        /// </summary>
        private void updateHealthPictures()
        {
            if (health <= 100 && health > 75)
                healthPicture.Texture = TextureManager.SetTexture("HealthFaces/100Health");
            else if (health <= 75 && health > 50)
                healthPicture.Texture = TextureManager.SetTexture("HealthFaces/75Health");
            else if (health <= 50 && health > 25)
                healthPicture.Texture = TextureManager.SetTexture("HealthFaces/50Health");
            else if (health <= 25 && health > 10)
                healthPicture.Texture = TextureManager.SetTexture("HealthFaces/25Health");
            else if (health <= 10)
                healthPicture.Texture = TextureManager.SetTexture("HealthFaces/10Health"); 
        }

        

        private void handleInput(GameTime gameTime)
        {                          
               if (Input.KeyDown(Keys.Up) || Input.KeyDown(Keys.W))  
                  velocity = new Vector2(0, -3);
               else if (Input.KeyDown(Keys.Down) || Input.KeyDown(Keys.S))
                 velocity = new Vector2(0, 3);
               else 
                 velocity = Vector2.Zero;
              
               if (Input.KeyDown(Keys.D))//right
                      Position += new Vector2 (2,0);
               if (Input.KeyDown(Keys.A))//left
                  Position += new Vector2(-2, 0);

               if (Input.KeyPressed(Keys.R))
                   weapon.reload();
               if (Input.KeyPressed(Keys.G))
                   throwGrenade();
               if (Input.KeyPressed(Keys.O))
               {
                   var a = new HealthSkill(this);
               }


            if (Input.KeyDown(Keys.D1) && !(weapon is Pistol))
                  {
                      Weapon temp = new Shotgun(weapon);//The current weapon.
                      weapon = new Pistol(prevWeapon);//Switch to the prev weapon.
                      prevWeapon = temp;//set the prev weapon to the current.
                      Texture = TextureManager.SetTexture ("Player/Player_Pistol");
                 }

               if (Input.KeyDown(Keys.D2) && !(weapon is Shotgun))
                 {   
                      Weapon temp = new Pistol(weapon);//hold a copy of current weapon.
                      weapon = new Shotgun(prevWeapon);//Switch to the prev weapon.
                      prevWeapon = temp;//set the prev weapon to the current.
                      Texture = TextureManager.SetTexture( "Player/Player_Shotgun");
                 }                        

             Position += velocity;

             MouseState mouseState = Mouse.GetState();
             Vector2 worldcoords = Camera.ToWordCoords(position);
             Vector2 toMouse = new Vector2(mouseState.X - worldcoords.X, mouseState.Y - worldcoords.Y);

             rotation = (float)Math.Atan2(toMouse.Y, toMouse.X);
        }
        #endregion

        private void throwGrenade()
        {
            Grenade g = new Grenade(20, 4, 5);
            Vector2 mousePos = Input.MousePosition;
            g.StartPosition = new Vector2(Position.X, Position.Y);
            g.Position = g.StartPosition;
            g.EndPosition = mousePos;
            g.Texture = TextureManager.SetTexture("grenade");

            //float XDistance = mousePos.X - g.Position.X;
          //  float YDistance = mousePos.Y - g.Position.Y;
        //    g.Velocity = new Vector2(XDistance,YDistance);
            
         
            grenades.Add(g);
        }

        


        #region Draw Methods
        public override void  Draw(SpriteBatch sb)
        {
            if (Alive) 
            {
                sb.Begin(SpriteSortMode.Deferred, null, null, null, null, null, Camera.Transform);
                sb.Draw(texture, position, null, Color.White, rotation, origin, 1.0f, SpriteEffects.None, 0.0f);    
                sb.End();
                ScreenWriter.Write("Experience Points: " + ExperiencePoints, new Vector2(20, 500), Color.Black);
                ScreenWriter.Write("Level: " + Level, new Vector2(20, 600), Color.Black);
                weapon.Draw(sb);                
            }
            foreach (Grenade g in grenades)
                g.Draw(sb, Camera.Transform);
        }
        public void drawCrosshair(SpriteBatch sb)
        {
            sb.Begin();
            sb.Draw(crosshair, new Vector2(Input.MousePosition.X - Player.crosshair.Width / 2, Input.MousePosition.Y - crosshair.Height / 2), Color.White);
            sb.End();
        }
        #endregion


        
    }


    public class PlayerStatistics
    {
        public int Level { get; set; }
        public int ExperiencePoints { get; set; }
        public int MaxHealth { get; set; }
        public float ReloadTime { get; set; }
        public float ScoreMultiplier { get; set; }
        public float FireRate { get; set; }
        public float Speed { get; set; }
        public float BulletDamage { get; set; }
        
    }
}
