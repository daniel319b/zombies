﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZombiesEngine.GameObjects.Player
{
    public static class Upgrades
    {

    }

    delegate void UpgradePlayer(PlayerUpgrade upgrade);

    class UpgradesEffects
    {
        public static Dictionary<string, UpgradePlayer> effects = new Dictionary<string, UpgradePlayer>()
        {
            { "Health", UpgradeHealth },
            { "Weapon Damage", UpgradeWeaponDamage },
            { "Grenade Damage", UpgradeGranadeDamage },
            { "Reload Speed", UpgradeReloadSpeed }
        };

        static void UpgradeHealth(PlayerUpgrade upgrade)
        {
            //int maxhealth = Session.Singleton.Player.CurrentStatistics.MaxHealth;
            //maxhealth += (int)(maxhealth + maxhealth * upgrade.UpgradePercent);
            //Session.Singleton.Player.CurrentStatistics.MaxHealth = maxhealth;
        }

        static void UpgradeWeaponDamage(PlayerUpgrade upgrade)
        {

        }

        static void UpgradeGranadeDamage(PlayerUpgrade upgrade)
        {

        }

        static void UpgradeReloadSpeed(PlayerUpgrade upgrade)
        {

        }
    }
}
