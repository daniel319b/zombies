﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZombiesEngine
{
    public class Skill
    {
        #region Properties

        /// <summary>
        /// The minimum level that the play has to be to get this skill.
        /// </summary>
        public int PlayerMinimumLevel { get; set; }

        /// <summary>
        /// The skill level.
        /// </summary>
        public int SkillLevel { get; set; }

        /// <summary>
        /// The name of this skill.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// What is this skill, what does it do?
        /// </summary>
        public string Description { get; set; }

        public Player Player { get; set; }

        #endregion


        public Skill(Player player)
        {
            Player = player;
        }
    }

    public class AmmoSkill : Skill
    {
        public AmmoSkill(Player player) : base(player)
        {
            player.Weapon.Magazines += 2;
        }
    }

    public class HealthSkill : Skill
    {
        public HealthSkill(Player player) : base(player)
        {
            player.MaxHealth += 10;
        }

        
    }
}
