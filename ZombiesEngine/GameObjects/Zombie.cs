﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace ZombiesEngine
{
    public class Zombie : GameObject
    {
        #region Fields and Properties
        
         float speed;
         int damage;        

        protected TimeSpan zombieAttack,zombieHit;
        protected  int START_HEALTH = 3;
        
        /// <summary>
        /// Indicates whether to draw a health bar ot not.
        /// </summary>
        public static bool HealthBarEnabled = true;
        static Texture2D healthBar,BloodAnimation;

         List<Animation> bloodAnimations = new List<Animation>();
  
        /// <summary>
        /// The damage of the zombie.
        /// </summary>
        public int Damage
        {
            get { return damage; }
            set { damage = value > 0 ? value : 1;}
        }

        /// <summary>
        /// The speed of the zombie.
        /// </summary>
        public float Speed
        {
            get { return speed; }
            set { speed = value > 0 ? value : 0; }
        }

        public bool Hit { get; set; }
        #endregion


        #region Initialization
        public Zombie()
        {
            health = START_HEALTH;
            speed = 1.3f;       
            zombieAttack = zombieHit = TimeSpan.Zero;
            damage = 1;
            Hit = false;
            healthBar = TextureManager.SetTexture("Zombie/ZombieHealthBar");
            BloodAnimation = TextureManager.SetTexture("Zombie/BloodSheet");
        }
        #endregion


        #region Update Methods
        public virtual  void Update(GameObject player,GameTime gameTime)
        {                    
            GoToPlayer(player,gameTime,40f);

            if (Hit)
            {
                addAnimation();
                Hit = false;
            }

            foreach (Animation a in bloodAnimations)
                a.Update(gameTime);
                 
            base.Update();
        }

        private void addAnimation()
        {
            Animation HitAnimation = new Animation(BloodAnimation, 4, 2, 12);
            HitAnimation.Position = getHitAnimationPosition();
            bloodAnimations.Add(HitAnimation);
        }   
       
        
        private void GoToPlayer(GameObject obj, GameTime gameTime,float threshold)
        {
            float XDistance = obj.Position.X - Position.X;
            float YDistance = obj.Position.Y - Position.Y;
            velocity = new Vector2(XDistance, YDistance);

            if (velocity.Length() < threshold)
            {
                velocity = Vector2.Zero;
                if(obj is Player)
                   hitPlayer((Player)obj,gameTime,damage);
            }
            else
            {
                velocity.Normalize();
                velocity = Vector2.Multiply(velocity, speed);
            }
             rotation = (float)Math.Atan2(YDistance, XDistance);

            Position += velocity;          
        }

        protected  virtual  void hitPlayer(Player player,GameTime gameTime,int damageToPlayer)
        {                      
            if (TimePassed(gameTime,1,ref zombieAttack))
                player.Health -= damageToPlayer;
        }
      
        #endregion

        

        #region Draw Methods
        public override void Draw(SpriteBatch sb)
        {
            if (Alive)
            {
                sb.Begin(SpriteSortMode.Deferred, null, null, null, null, null, Camera.Transform);
                sb.Draw(texture, position, null, Color.White, rotation, origin, 1.0f, SpriteEffects.None, 0.0f);
                sb.End();

                if (HealthBarEnabled)
                    drawHealthBar(sb, START_HEALTH);
            }

            foreach(Animation a in bloodAnimations)
                a.Draw(sb, Rotation, 0.3f, Origin, Camera.Transform);     
              
        }

        private Vector2 getHitAnimationPosition()
        {
            //the center of the object.
            Vector2 center = (Position - Origin) + new Vector2(Texture.Width / 2, Texture.Height / 2);
            float distance = -125f;//how much you want to push.
            //calculates the new vector with the push.
            Vector2 offset = new Vector2((float)(Math.Cos(Rotation)), (float)(Math.Sin(Rotation))) * distance;
            //add the push to the center.
            offset += center;
            return offset;
        }

        protected virtual void drawHealthBar(SpriteBatch sb,int startHealth)
        {
            float angle = MathHelper.ToDegrees(rotation);
            Rectangle healthDest = new Rectangle(BoundBox.X, BoundBox.Y, healthBar.Width * health / startHealth, healthBar.Height);
            sb.Begin(SpriteSortMode.Deferred, null, null, null, null, null, Camera.Transform);
            sb.Draw(healthBar, Position,healthDest,Color.White,MathHelper.ToRadians(angle - 90),Origin,1,SpriteEffects.None,0);
            sb.End();
        }
        #endregion



        #region Methods 
        /// <summary>
        /// Hits the zombie with some damage.
        /// </summary>
        /// <param name="damage">The damage to take off the zombie's health.</param>
        public void takeDamage(int damage)
        {
            health -= damage;
            Hit = true;
        }

        private bool TimePassed(GameTime gameTime, float duration, ref TimeSpan t)
        {
            TimeSpan current = TimeSpan.FromSeconds(duration);
            if (gameTime.TotalGameTime - t > current)
            {
                t = gameTime.TotalGameTime;
                return true;
            }
            return false;
        }
       
        /// <summary>
        /// Applys the flocking behavior to the zombies.
        /// </summary>
        /// <param name="other">An other zombie to check with</param>
        /// <param name="threshold">The distance that the each zombie will keep from his friends.</param>
        public void Flock(Zombie other,float threshold)
        {
            if (this != other)
            {
                Vector2 pushDirection = this.Position - other.Position;
                if (pushDirection.Length() < threshold)
                {
                    this.velocity += pushDirection;
                    this.velocity.Normalize();
                    this.Position += this.velocity;
                }
            }
        }

        /// <summary>
        /// Hits the zombie.
        /// </summary>
        /// <param name="b">The bullet that hits the zombie.</param>
        /// <param name="player">The player of the game.</param>
        public void hit(Bullet b, Player player)
        {
            b.Alive = false;//kill the bullet;
            takeDamage(1);
            if (Health == 0)
            {
                Alive = false;
                player.ZombiesKilled++;
                if (this is BigZombie)
                {
                    player.ScoreToAdd = 15;
                    player.Score += player.ScoreToAdd;
                    MessageManager.addNewScoreMessage("+" + player.ScoreToAdd, this);
                    player.ExperiencePoints += player.ScoreToAdd;
                    //MessageManager.AddMessage("+"+5);
                }
                else
                {
                    player.ScoreToAdd = 10;
                    player.Score += player.ScoreToAdd;
                    MessageManager.addNewScoreMessage("+" + player.ScoreToAdd, this);
                    player.ExperiencePoints += player.ScoreToAdd;
                }
                PowerUpManager.AddPowerUp(this, player);
            }
        }
        #endregion


    }
}