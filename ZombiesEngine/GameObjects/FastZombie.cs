﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZombiesEngine
{
    public class FastZombie : Zombie
    {
        public FastZombie()
        {
            Speed = 2.8f;
            Damage = 2;
            START_HEALTH = 3;
            health = START_HEALTH;
        }
    }
}
