﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ZombiesEngine
{
    public class BigZombie : Zombie
    {      
      
        public BigZombie()
        {
            Speed = 0.7f;//The speed of the zombie
            Damage = 5;//The damage to the player by the zombie
            START_HEALTH = 5;//The start health
            Health = START_HEALTH;
        }           
      
    }
}
