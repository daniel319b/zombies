﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace ZombiesEngine
{
    public class Pistol : Weapon 
    {
        public Pistol(Player player)
            :base(player)
        {         
            fireRate = 0.60f;
            bulletsCount = 72;
            Magazines = 6;
            base.initAmmo();
        }

        public Pistol(Weapon weapon) : base(weapon) { }
    }
}
