﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using ScreenSystem;

namespace ZombiesEngine
{
    public class Shotgun : Weapon 
    {
        public Shotgun(Player player)
            : base(player)
        {
            fireRate = 1.2f; 
            Magazines = 6;
            bulletsCount = 30;
            base.initAmmo();
        }

        public Shotgun(Weapon weapon) : base(weapon) { }

        protected override void handleShooting(GameTime gameTime)
        {
            if (BulletsLeft == 0)//if you dont have any bullets, you cant shoot.
            {
                if (Magazines == -1)//to prevent bugs...
                    Magazines = 0;
                return;
            }

            if (Input.KeyDown(Keys.Space) || Input.LeftButtonDown ())//If the user clicked the fire button,Fire!
            {             
                if (fireTime > fireRate)
                {                    
                    fireTime = 0;//zero the fire time;
                    //make the shotgun effect
                    GenerateMultiShot(shooter, MathHelper.ToRadians(10), 6, 10);
                    BulletsLeft--;    
                }
            }
            fireTime += (float)gameTime.ElapsedGameTime.TotalSeconds;//count the number of seconds passed.
        }
       
      }
    }

