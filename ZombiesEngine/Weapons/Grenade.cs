﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ScreenSystem;

namespace ZombiesEngine
{
    public class Grenade : GameObject
    {
        #region Fields and Properties

        private float timer;

        /// <summary>
        /// This event is fired when the grenade should explode.
        /// </summary>
        public event EventHandler Explode;

      
        /// <summary>
        /// The time for the grenade to explode.
        /// </summary>
        public float ExplodeTime
        { get; set; }

        /// <summary>
        /// The time left for the grenade to explode.
        /// </summary>
        public float TimeLeftToExplode
        {
            get { return ExplodeTime - timer; }
        }

        /// <summary>
        /// The damage dealt when very close to the explosion.
        /// </summary>
        public int FullHitDamage
        { get; set; }

        /// <summary>
        /// The damage dealt when not so close to the explosion.
        /// </summary>
        public int HalfHitDamage
        {
            get { return FullHitDamage / 2; }
        }

        /// <summary>
        /// The Area Of Effect radius.
        /// </summary>
        public int AoERadius { get; set; }

        /// <summary>
        /// Half of the Area Of Effect.
        /// </summary>
        public int HalfAoERadius { get; set; }
        #endregion

        #region Constructor
        public Grenade(int hitRadius, int hitDamage,float explodeTime)
        {
            FullHitDamage = hitDamage;
            AoERadius = hitRadius;
            HalfAoERadius = hitRadius / 2;
            ExplodeTime = explodeTime;
        }
        #endregion

        public Vector2 EndPosition;
        public Vector2 StartPosition;

        public void Update(GameTime gameTime)
        {
            base.Update();

            //float XDistance = EndPosition.X - StartPosition.X;
            //float YDistance = EndPosition.Y - StartPosition.Y;

            //velocity = new Vector2(XDistance, YDistance);

            //if (velocity.Length() < 10)
            //    velocity = Vector2.Zero;
            //else
            //{
            //    velocity.Normalize();
            //    position += velocity;
            //}

            GoToPlayer(new GameObject(EndPosition), gameTime, 20);
            handleExplosion(gameTime);
        }

        private void GoToPlayer(GameObject obj, GameTime gameTime, float threshold)
        {
            float XDistance = obj.Position.X - Position.X;
            float YDistance = obj.Position.Y - Position.Y;
            velocity = new Vector2(XDistance, YDistance);

            if (velocity.Length() < threshold)
            {
                velocity = Vector2.Zero;

            }
            else
            {
                velocity.Normalize();
                velocity = Vector2.Multiply(velocity, 2);
            }
            rotation = (float)Math.Atan2(YDistance, XDistance);

            Position += velocity;
        }
        private void handleExplosion(GameTime gameTime)
        {
            timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (timer >= ExplodeTime)
            {
                if (Explode != null)
                    Explode(this, EventArgs.Empty);
                timer = 0;
            }
        }

        #region Draw
        public override void Draw(SpriteBatch sb, Matrix transform)
        {
            base.Draw(sb, transform);
            sb.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, transform);
            sb.DrawString(Font.Regular, (int)TimeLeftToExplode+"", new Vector2(boundBox.Center.X, boundBox.Top), Color.Black);
            sb.End();
        }
        #endregion
    }
}
