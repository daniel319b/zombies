﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ScreenSystem;

namespace ZombiesEngine
{
    /// <summary>
    /// Represents a Weapon in the game.
    /// </summary>
    public class Weapon
    {
        #region FIelds and Properties

        protected float fireTime, fireRate;//the fire rate of the weapon.
        protected int bulletsCount;
        protected Player shooter;//Who shot the bullet.

        private List<Bullet> bullets;
        private int bulletsPerMag;       
        private int bulletsLeft;
        private int magazines;

        /// <summary>
        /// The list of the active bullets.
        /// </summary>
        public List<Bullet> Bullets
        {
            get { return bullets; }
            set { bullets = value; }
        }
       
        /// <summary>
        /// The number of bullets left in the weapon's magazine.
        /// </summary>
        public int BulletsLeft
        {
            get { return bulletsLeft; }
            set { bulletsLeft = value; }
        }
       
        /// <summary>
        /// The current number of magazines in the weapon.
        /// </summary>
        public int Magazines
        {
            get { return magazines; }
            set { magazines = value; }
        }
       
        /// <summary>
        /// The number of bullets per magazine.
        /// </summary>
        public int BulletsPerMag
        {
            get { return bulletsPerMag; }
            set { bulletsPerMag = value; }
        }

        #endregion

        #region Constructors and Initialization

        /// <summary>
        /// Creates a new weapon.
        /// </summary>
        /// <param name="player">The player who shoots the weapon.</param>
        public Weapon(Player player)
        {
            shooter = player;
            bullets = new List<Bullet>();  
        }

        /// <summary>
        /// A copy constructor.
        /// </summary>
        /// <param name="weapon">The weapon to copy.</param>
        public Weapon(Weapon weapon)
        {
            shooter = weapon.shooter;
            bullets = new List<Bullet>();
            magazines = weapon.magazines;
            bulletsCount = weapon.bulletsCount;
            bulletsPerMag = weapon.bulletsPerMag;
            fireRate = weapon.fireRate;
            bulletsLeft = weapon.bulletsLeft;
        }
        /// <summary>
        /// Initializes the ammo of the weapon.
        /// </summary>
        protected void initAmmo()
        {
            bulletsPerMag = bulletsCount / magazines;
            bulletsLeft = bulletsPerMag;
        }

        #endregion

        #region Update and Draw
        /// <summary>
        /// Updates the bullets and handles the shooting.
        /// </summary>
        /// <param name="gameTime">A gameTime object.</param>
        public virtual void Update(GameTime gameTime)
        {
            handleShooting(gameTime);
            checkInactiveBullets();//Delete inactive bullets.

            foreach (Bullet b in bullets)
                b.Update();  
        }

        /// <summary>
        /// Draws the weapon and the bullets.
        /// </summary>
        public virtual void Draw(SpriteBatch sb)
        {
            foreach (Bullet b in bullets)
                         b.Draw(sb);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Handles the shooting when the player clicks the shoot button.
        /// </summary>
        protected virtual void handleShooting(GameTime gameTime)
        {
            if (bulletsLeft == 0)//if you dont have any bullets, you cant shoot.
            {
                if (magazines == -1)//to prevent bugs...
                    magazines = 0;
                return;
            }

            if (Input.KeyDown(Keys.Space) || Input.LeftButtonDown())//If the user clicked the fire button,Fire!
            {
                if (fireTime > fireRate)
                {
                    fireTime = 0;//zero the fire time;
                    createBullet();//add a bullet to the game.           
                    bulletsLeft--;//Shot 1 bullet.  
                }
            }

            fireTime += (float)gameTime.ElapsedGameTime.TotalSeconds;//count the number of seconds passed.   
        }

        /// <summary>
        /// Creates a new bullet and fires it.
        /// </summary>
        protected virtual void createBullet()
        {
            Bullet b = new Bullet();//creates a bullet.  
            b.Position = new Vector2(shooter.Position.X, shooter.Position.Y);//sets the position to the player's position         
            b.Velocity = new Vector2((float)Math.Cos(shooter.Rotation), (float)Math.Sin(shooter.Rotation)) * 8;//set the velocity according to the rotation.                      
            bullets.Add(b);//add the bullet to the bullet list.
        }

        /// <summary>
        /// Checks for inactive bullets and deletes them.
        /// </summary>
        private void checkInactiveBullets()
        {
            List<Bullet> inactiveBullets = new List<Bullet>(bullets.Count);//A list that hold the inactive bullets.
            foreach (Bullet b in bullets)//Updates every bullet .
            {
                b.Update();
                if (!b.Alive) inactiveBullets.Add(b);//add the dead bullet to the inactive bullets list.
            }

            foreach (Bullet b in inactiveBullets)  //Removes inactive bullets from the list
            {
                bullets.Remove(b);
            }
            inactiveBullets.Clear();
        }

        /// <summary>
        /// Reloads the weapon.
        /// </summary>
        public void reload()
        {
            if (magazines == 0 || (magazines == 0 && bulletsLeft == 0))//if you have no bullets left, so you cant reload.
                return;
            bulletsLeft += bulletsPerMag;//add the left bullets to the new magazine..
            magazines--;
        }

        /// <summary>
        /// Generates a multi shot with a number of bullets and a specified shot angle.
        /// </summary>
        /// <param name="source">The shooter player object.</param>
        /// <param name="shotangle">The angle of the multishot.</param>
        /// <param name="numOfBullets">the number of bullets you want to be shot.</param>
        /// <param name="distanceFromObject">The distance from center of the object that the bullets will be released.</param>
        public void GenerateMultiShot(Player source, float shotangle, int numOfBullets, float distanceFromObject)
        {
            Vector2 center = (source.Position - source.Origin) + new Vector2(source.Texture.Width / 2, source.Texture.Height / 2);  //Where our ship/player/object is

            float anglebetweenshots = shotangle / (float)(numOfBullets - 1);  //Gets the angle between any two shots.
            float minAngle = source.Rotation - (anglebetweenshots * (numOfBullets / 2.0f));  //Finds the minimum angle that any shot will be using.
            float maxAngle = source.Rotation + (anglebetweenshots * (numOfBullets / 2.0f));  //Finds the maximum angle that any shot will be using.

            Vector2 offset;  //Offset vector so we can push the bullet from the center of our game object (player, ship, or whatever)
            Bullet b;  //Bullet object so we can add it to out bullets list.
            float speed = 5;  //Speed of the bullets


            for (float deltaangle = minAngle; deltaangle < maxAngle; deltaangle = deltaangle + anglebetweenshots)
            {
                offset = new Vector2((float)(Math.Cos(deltaangle)), (float)(Math.Sin(deltaangle))) * distanceFromObject;
                offset += center;

                b = new Bullet();  //Make a new bullet with the provided information
                b.Position = offset;
                b.Velocity = new Vector2((float)(Math.Cos(deltaangle)), (float)(Math.Sin(deltaangle))) * speed;  //Set the velocity to the deltaangle angle

                bullets.Add(b);  //Add the bullet to the main bullets list.
            }
        }
        #endregion
    }
}
