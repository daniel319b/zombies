﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScreenSystem;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ZombiesEngine;

namespace Zombies
{
    public class MainMenuScreen : MenuScreen
    {
        ZombieSpawner spawner;

        #region Initialization
        /// <summary>
        /// Constructor that initializes all the menu entries in the screen.
        /// </summary>
        public MainMenuScreen() 
            :base("Main Menu")
        {
            //Create the menu entries.
            MenuEntry playGameMenuEntry = new MenuEntry("Play");
            MenuEntry exitGameMenuEntry = new MenuEntry("Exit");

            //Hook up the events for the entries.
            playGameMenuEntry.Selected += PlayGameMenuEntrySelected;
            exitGameMenuEntry.Selected += OnCancel;

            //Add the entries to the list.
            MenuEntries.Add(playGameMenuEntry);
            MenuEntries.Add(exitGameMenuEntry);
           
        }

        /// <summary>
        /// Load Fonts and Textures.
        /// </summary>
        public override void LoadContent()
        {
            Font.MenuEntry = ScreenManager.Content.Load<SpriteFont>("menufont");
            Font.Title = ScreenManager.Content.Load<SpriteFont>("TitleFont");
            spawner = new ZombieSpawner(ScreenManager.Viewport);
            Input.MouseTexture = ScreenManager.Content.Load<Texture2D>("Mouse");
        }
        #endregion

        #region Update and Draw
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, false, false);
            spawner.Update(gameTime,new GameObject (Input.MousePosition));
            Zombie.HealthBarEnabled = false;
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);       
            spawner.Draw(ScreenManager.SpriteBatch);
        }
        #endregion

        #region Handling Input

        /// <summary>
        /// Event handler for when the Play Game menu entry is selected.
        /// </summary>
        void PlayGameMenuEntrySelected(object sender,EventArgs e)
        {
            LoadingScreen.Load(ScreenManager, true, new MainGameScreen());
        }

        protected override void OnCancel()
        {
            ScreenManager.Game.Exit();
        }

        #endregion

    }
}
