﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Threading;
using ScreenSystem;
using ZombiesEngine;

namespace Zombies
{
    /// <summary>
    /// This is the main game class.
    /// </summary>
    class MainGameScreen : Screen
    {         
        ContentManager Content;

        Player player;  
        Texture2D background;     
        ZombieSpawner spawner;
        private float pauseAlpha;
        private bool pause = false;

        public MainGameScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(0.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);  
        }
       
        #region Load & Unload Content
        public override void LoadContent()
        {
            if (Content == null)
                Content = new ContentManager(ScreenManager.Game.Services, "Content");
            //make a loading effect.
            Thread.Sleep(1000);
            //reset the time.
            ScreenManager.Game.ResetElapsedTime();
            Camera.ScreenBounds = ScreenManager.Viewport.Bounds;


            player = new Player();            
            //sets the player to the middle of the screen
            Vector2 middleScreen = new Vector2((ScreenManager.Viewport.Width / 2) - player.Texture.Width / 2, (ScreenManager.Viewport.Height / 2) - player.Texture.Height / 2);
            player.Position = middleScreen;
            HUD.Player = player;
            background = Content.Load<Texture2D>("Backgrounds/SnowBackground");           
            Font.Regular = Content.Load<SpriteFont>("font");
            spawner = new ZombieSpawner(ScreenManager.Viewport);
            ScreenWriter.SpriteBatch = ScreenManager.SpriteBatch;
        }

        public override void UnloadContent()
        {
            Content.Unload();
        }
        #endregion

        
        #region Update
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
 	        base.Update(gameTime, otherScreenHasFocus, false);
            #region Pause and Unpause logic
            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
            {
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
                pause = true;
            }
            else
            {
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);
                pause = false;
            }

            if (pause)//if the game is pause, no need to update.
                return;
            #endregion

            if (Input.KeyPressed(Keys.Escape))          
                    ScreenManager.AddScreen(new PauseMenuScreen());  
              
            Zombie.HealthBarEnabled = true;
            player.Update(gameTime);//Updates the player.
            spawner.Update(gameTime, player);//Updates the zombies
          
            foreach (Bullet b in player.Weapon.Bullets)
            {
                //check if the bullet is out of the screen bounds.         
                if (!Camera.InView(b.BoundBox))
                    b.Alive = false;
                foreach (Zombie z in spawner.Zombies)
                    if (b.BoundBox.Intersects(z.BoundBox))   
                        z.hit(b, player);             
            }
       
            OnDeath();   
  
            MessageManager.Update();
            PowerUpManager.Update(player,gameTime);
            Input.Update();//Updates the Input.
        }
 
        #endregion


        #region Draw Methods
        public override void  Draw(GameTime gameTime)
        {
            ScreenManager.GraphicsDevice.Clear(Color.CornflowerBlue);
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;

            drawBackground(spriteBatch);
            PowerUpManager.Draw(spriteBatch);
            player.Draw(spriteBatch);          
            spawner.Draw(spriteBatch);   
            HUD.Draw(spriteBatch);
            MessageManager.Draw(spriteBatch);
            if (!pause)//draw the crosshair only if the game is not paused.
                player.drawCrosshair(spriteBatch);

            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 1.5f);
                ScreenManager.FadeBackBufferToBlack(alpha);
            }
  
        }

        private void drawBackground(SpriteBatch spriteBatch)
        {
            for(int i=-4;i<4;i++)
                for (int j = -4; j < 4; j++)
                {
                    spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null, Camera.Transform);
                    spriteBatch.Draw(background, new Vector2 (j*background.Width,i*background.Height), Color.White);
                    spriteBatch.End();
                }
        }
        #endregion


        #region Methods

        /// <summary>
        /// Handles the death of the player.
        /// </summary>
        private void OnDeath()
        {
            if (player.Health <= 0)
            {
                player.Alive = false;

                ScreenManager.AddScreen(new GameOverScreen(player));
                spawner.Zombies.Clear();
            }
        }   
        #endregion
    }
}
