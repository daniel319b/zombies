﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScreenSystem;

namespace Zombies
{
    /// <summary>
    /// The pause menu comes up over the top of the game,
    /// giving the player options to resume or quit.
    /// </summary>
    public class PauseMenuScreen  : MenuScreen
    {
        #region Initialization

        public PauseMenuScreen(): base("Pause")
        {
            // Create our menu entries.
            MenuEntry resumeGameMenuEntry = new MenuEntry("Resume Game");
            MenuEntry quitGameMenuEntry = new MenuEntry("Quit Game");

            // Hook up menu event handlers.
            resumeGameMenuEntry.Selected += OnCancel;
            quitGameMenuEntry.Selected += new EventHandler(quitGameMenuEntry_Selected);

            // Add entries to the menu.
            MenuEntries.Add(resumeGameMenuEntry);
            MenuEntries.Add(quitGameMenuEntry);
        }       
        #endregion

       
        #region Handle Input
        /// <summary>
        /// Event handler for when the user selects the "Quit game" option.
        /// This uses the loading screen to transition from the game back to the main menu screen.
        /// </summary>
        void quitGameMenuEntry_Selected(object sender, EventArgs e)
        {
            LoadingScreen.Load(ScreenManager, false, null, new BackgroundScreen(),
                                                          new MainMenuScreen());
        }
        #endregion
    }
}
