﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScreenSystem;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ZombiesEngine;

namespace Zombies
{
    /// <summary>
    /// This screen appears when the player dies and the game is over.
    /// it shows some stats about the player like how many zomies did he kil and how much time did he survive.
    /// </summary>
    public class GameOverScreen : MenuScreen
    {
        #region Fields
        private Player player;
        private Texture2D skull;
        #endregion

        #region Initialization

        public GameOverScreen(Player player):base("Game Over")
        {         
            this.player = player;

            // Create our menu entries.
            MenuEntry playAgainGameMenuEntry = new MenuEntry("Play Again");
            MenuEntry quitGameMenuEntry = new MenuEntry("Return to Main Menu");

            // Hook up menu event handlers.
            playAgainGameMenuEntry.Selected += OnCancel;
            quitGameMenuEntry.Selected += new EventHandler(quitGameMenuEntry_Selected);

            // Add entries to the menu.
            MenuEntries.Add(playAgainGameMenuEntry);
            MenuEntries.Add(quitGameMenuEntry);
        }

        public override void LoadContent()
        {
            skull = TextureManager.SetTexture("HealthFaces/skull");
        }
        #endregion

        #region Handle Input
        protected override void OnCancel()
        {
            ExitScreen();
            player.ResetPlayer();
        }

       
        void quitGameMenuEntry_Selected(object sender, EventArgs e)
        {
            LoadingScreen.Load(ScreenManager, false, null, new BackgroundScreen(),
                                                          new MainMenuScreen());
        }
        #endregion
      
        #region Draw
        public override void Draw(GameTime gameTime)
        {
            Vector2 skullStartPos = new Vector2(MenuEntries[1].Position.X, MenuEntries[1].Position.Y + 50);
            Vector2 textStartPos = new Vector2(skullStartPos.X + skull.Width + 15, skullStartPos.Y+15);
            string stats = getPlayerStats();

            //Very important, not to get bugs!
            Font.Regular = ScreenManager.Content.Load<SpriteFont>("font");
            ScreenManager.SpriteBatch.Begin();
            ScreenManager.SpriteBatch.Draw(skull, skullStartPos, Color.White* TransitionAlpha);
            ScreenManager.SpriteBatch.DrawString(Font.Regular, stats, textStartPos, Color.Red * TransitionAlpha);
            ScreenManager.SpriteBatch.End();
            base.Draw(gameTime);
        }

        /// <summary>
        /// Gets the stats of the player.
        /// </summary>
        /// <returns>Returns a string which contains all the stats of the player.</returns>
        private string getPlayerStats()
        {
            string stats = getTimeSurvived((int)player.TimeAlive) + Environment.NewLine + Environment.NewLine +
                            "Zombies Killed: " + player.ZombiesKilled + Environment.NewLine + Environment.NewLine +
                            "Score :" + player.Score;
            return stats;
        }

        /// <summary>
        /// Converts the seconds to a sentence.
        /// </summary>
        /// <param name="time">The time.</param>
        private string getTimeSurvived(int time)
        {
            if (time < 2)
                return "1 Second";
            else if (time <= 60)
                return time + " Seconds";
            else
            {
                int minutes = time / 60;
                int seconds = time % 60;
                if (minutes == 1)
                {
                    if(seconds == 1)
                        return "1 Minute and 1 Second";
                    return "1 Minute and " + seconds + " Seconds";
                }
                else
                    if(seconds == 1)
                        return minutes + " Minutes and " + "1 Second";
                return minutes + " Minutes and " + seconds + " Seconds";
            }
        }
        #endregion
    }
}
