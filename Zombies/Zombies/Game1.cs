using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Microsoft.Xna.Framework;
using ScreenSystem;
using Microsoft.Xna.Framework.Graphics;
using ZombiesEngine;

namespace Zombies
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        ScreenManager screenManager;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = 720;
            graphics.PreferredBackBufferWidth = 1280;
            Content.RootDirectory = "Content";
            this.Window.Title = "Daniel's Zombie Game";

            //Create the screen manager component.
            screenManager = new ScreenManager(this);
            Components.Add(screenManager);

            //Add the MainMenu screen and the background screen.
            screenManager.AddScreen(new BackgroundScreen());
            screenManager.AddScreen(new MainMenuScreen());

         //   IsMouseVisible = true;
            
        }
        protected override void Initialize()
        { 
            TextureManager.Content = Content;
            base.Initialize(); 
        }

       
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            //All the drawing is happening in the screen manager.
            base.Draw(gameTime);
        }

    }
}
